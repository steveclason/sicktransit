<?php
/**
 * Adds Gutenberg features to the theme.
 *
 * @link https://wordpress.org/gutenberg/handbook/reference/theme-support/
 */

function sicktransit_gutenberg_features() {
  add_theme_support( 'editor-color-palette',
      array(
          'name' => 'strong magenta',
          'color' => '#a156b4',
      ),
      array(
          'name' => 'light grayish magenta',
          'color' => '#d0a5db',
      ),
      array(
          'name' => 'very light gray',
          'color' => '#eee',
      ),
      array(
          'name' => 'very dark gray',
          'color' => '#444',
      )
    );

  add_theme_support( 'align-wide' );

  // Enqueues the default block styles. 
  add_theme_support( 'wp-block-styles' );

}
add_action( 'after_setup_theme', 'sicktransit_gutenberg_features' );
