<?php
/*
Plugin Name:  Steve's Grid Shortcodes
Plugin URI:   https://developer.wordpress.org/plugins/the-basics/
Description:  Provides shortcodes for marking up a checkerboard-type grid.
Version:      0.0.1
Author:       Steve Clason
Author URI:   https://steveclason.com
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:
Domain Path:  /languages
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*
  Created a grid of paired elements, like image/text. Built for manipulation with CSS Flex and
  Grid. The idea is image/text pairs which can be alternated to form a checkerboard-like grid,
  and then the order changed when small viewports dictate a columnart presentation.
*/
 // [grid]
function grid_func( $atts, $content = null ) {
  $a = shortcode_atts( array(
      'foo' => 'something',
      'bar' => 'something else',
  ), $atts );

  return
    '<div class="grid">' .
      do_shortcode( $content ) .
    '</div>';
}
add_shortcode( 'grid', 'grid_func' );

function grid_pair_func( $atts, $content = null ) {
  $a = shortcode_atts( array(
      // 'foo' => 'something',
  ), $atts );

  return
    '<div class="grid__pair">' .
      do_shortcode( $content ) .
    '</div>';
}
add_shortcode( 'grid_pair', 'grid_pair_func' );

function grid_left_func( $atts, $content = null ) {
  $a = shortcode_atts( array(
      // 'foo' => 'something',

  ), $atts );

  return
    '<div class="grid__item grid__item--left">' .
      do_shortcode( $content ) .
    '</div>';
}
add_shortcode( 'grid_left', 'grid_left_func' );

function grid_right_func( $atts, $content = null ) {
  $a = shortcode_atts( array(
      // 'foo' => 'something',

  ), $atts );

  return
    '<div class="grid__item grid__item--right">' .
      do_shortcode( $content ) .
    '</div>';
}
add_shortcode( 'grid_right', 'grid_right_func' );
