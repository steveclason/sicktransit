
var gulp = require('gulp');
var sass = require( 'gulp-sass' );
var sourcemaps = require('gulp-sourcemaps');
// var rename = require( 'gulp-rename' );
// var minify = require( 'gulp-minify-css' );
var autoprefixer = require( 'gulp-autoprefixer' );
// var util = require( 'gulp-util' );
var log = require( 'fancy-log' );
//require( 'stylelint' )(),

gulp.task( 'sass', function () {
  'use strict';
  log( 'Generate CSS files ' + (new Date()).toString());
  return gulp
    .src('./src/themes/sicktransit/sass/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe( autoprefixer( 'last 4 version' ))
    .pipe( sourcemaps.write('.'))
    .pipe( gulp.dest( './src/themes/sicktransit' ))
    .pipe( gulp.dest( 'C:/Users/steve/Wordpress/wp-content/themes/sicktransit' ));
  // .pipe( rename({ suffix: '.min' }))
  // .pipe(minify())
  // .pipe(gulp.dest( './tilghman' ));
});

gulp.task( 'editor-sass', function () {
  'use strict';
  log( 'Generate CSS files ' + (new Date()).toString());
  return gulp
    .src('./src/themes/sicktransit/editor-sass/editor-style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe( autoprefixer( 'last 4 version' ))
    .pipe( sourcemaps.write('.'))
    .pipe( gulp.dest( './src/themes/sicktransit' ))
    .pipe( gulp.dest( 'C:/Users/steve/Wordpress/wp-content/themes/sicktransit' ));
  // .pipe( rename({ suffix: '.min' }))
  // .pipe(minify())
  // .pipe(gulp.dest( './tilghman' ));
});

// gulp.task('sass:watch', function () {
//   gulp.watch('./inn97win/sass/**/*.scss', ['sass']);
// });

gulp.task( 'copy', function() {
  'use strict';
  log( 'Deploy files to dist ' + (new Date()).toString());
  return gulp
    .src( './src/themes/sicktransit/**/', { base: './src/themes/sicktransit' } )
    .pipe( gulp.dest( './dist/themes/sicktransit' ))
    // .src( '.dist/themes/sicktransit/**/*', { base: '.dist/themes/sicktransit' } )
    .pipe( gulp.dest( 'C:/Users/steve/Wordpress/wp-content/themes/sicktransit' ));
});

gulp.task( 'deploy', [ 'sass', 'editor-sass', 'copy' ]);
