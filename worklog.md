# SickTransit Worklog #
An playground environment for WordPress theme and plugins, testing and evaluating Gutengerg page builder, shortcode creation, and so on. Also evaluating newer HTML/CSS developments and their use for layout.
That is far too ambitious for a single project, though.

## Workplan ##
1. Layout the basic \_s theme structure using CSS Grid.
2. Create fake content elements using Gutenburg, mimic a hotel or real estate agency (maybe).
3. Use a sub-grid for content elements.
4. Breakpoints something like 400px, 768px, 1280px, 1920px, maybe one more.

## Sat 21 Apr 2017 ##
* set up environment
* update Node and NPM
* create theme
* make repo
* layout grid styles for shortcodes
