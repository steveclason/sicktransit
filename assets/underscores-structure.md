# Underscores Structure #
Contains the html structure of the starter theme "\_s" ("Underscores").

- body
  - div.site #page
  - header.site-header #masthead
  - div.site-content #content
    - div.content-area #primary
      - main.site-main #main
        - article.hentry
          - header.entry-header
          - div.entry-content
          - footer.entry-footer
    - aside.widget-area #secondary
  - footer.site-footer #colophon
